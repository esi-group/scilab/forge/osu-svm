/*  sci_svm_class.cpp

    Scilab C++ code for SVM classification.

    This provides a Scilab interface of LIBSVM 2.33.

    Refer to http://www.csie.ntu.edu.tw/~cjlin for more information
      on LIBSVM.

    By Yi Zhao, OSU-EE, 01/20/2002 (version 2.00).
    By Yi Zhao, OSU-EE, 12/31/2001 (version 1.10).
    By Yi Zhao, OSU-EE, 12/28/2001 (version 1.00).

    Modifiedy by Junshui Ma, 02/12/2002 to fit in the 
	the OSU SVM framework.

    Scilab version by Y. Collette
*/

#include <stdio.h>

extern "C" {
#include <api_scilab.h>
#include <stack-c.h>
#include <sciprint.h>
#include <MALLOC.h>
#include <Scierror.h>
}

// LIBSVM
#include "svm.h"

// mzSVM common module
#include "mzSVMCommon.h"
#include "external_data.h"

//[ClassRate, DecisionValue, Ns, ConfMatrix, PreLabels]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias)
//[ClassRate, DecisionValue, Ns, ConfMatrix, PreLabels]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias, Parameters)
//[ClassRate, DecisionValue, Ns, ConfMatrix, PreLabels]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias, Parameters, nSV, nLabel)
//[ClassRate, DecisionValue, Ns, ConfMatrix, PreLabels]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias, Parameters, nSV, nLabel, Verbose)
//
// Inputs:
//    Samples  - patterns to be classified, MxN;
//    Labels   - labels of input patterns, 1xN;
//    AlphaY   - coef. of SVs, (L-1) x sum(nSV);
//    SVs      - SVs, M x sum(nSV);
//    Bias     - bias of classifier(s), 1 x L*(L-1)/2;
//    Parameters  -  parameters outputed from training.
//    nSV      - numbers of SVs in each class, 1xL;
//    nLabel      - labels of each class, 1xL;
//    verbose  - verbose level
//                0 : very silent (default);
//                1 : a little verbose;
//                2 : very verbose.
//
// Outputs:
//    ClassRate - the ration of properly classified pattern to the total number of
//            input patterns.
//    DecisionValue - the output of the decision function.
//    Ns - number of samples in each class.
//    ConfMatrix - Confusion Matrix, L+1 x L+1
//                  ConfMatrix(i,j) = P(X^ in j| X in i);
//    PreLabels - the predicated Labels

extern "C" int sci_svm_class(char * fname)
{
  svm_problem prob;
  svm_model model;
  svm_node * x_space;

  int * verbose_addr = NULL, * X_addr = NULL, * Y_addr = NULL;
  int nX, mX, nY, mY;
  double dbl_verbose, * pdbl_X = NULL, * pdbl_Y = NULL;
  double * pdbl_dLabels = NULL, * pdbl_dnSVs = NULL, * pdbl_SVs = NULL, * pdbl_alphaY = NULL, * pdbl_Bias = NULL, * pdbl_inPar = NULL;
  int * dLabels_addr = NULL, * dnSVs_addr = NULL, * SVs_addr = NULL, * alphaY_addr = NULL, * Bias_addr = NULL, * inPar_addr = NULL;
  int  mdLabels, mdnSVs, mSVs, malphaY, mBias, minPar;
  int  ndLabels, ndnSVs, nSVs, nalphaY, nBias, ninPar;
  double * pdbl_DV = NULL, * pdbl_CR = NULL, * pdbl_dNs = NULL, * pdbl_CM = NULL, * pdbl_PredLabel = NULL;
  int nClass, nPar, i, j, lTarget, lPredict;
  // Verbose level.
  int mzVerbose;
  SciErr _SciErr;

  // Test input arguments.
  if (Rhs < 6 || Rhs > 9)
    {
      Scierror(999,"%s: wrong number of input parameters.\n",fname);
      return 0;
    }

  // Get verbose level.
  if (Rhs < 9)
    {
      mzVerbose = 0;
    }
  else
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 9, &verbose_addr); SCISVM_ERROR;
      getScalarDouble(pvApiCtx, verbose_addr, &dbl_verbose);
      mzVerbose = (int) dbl_verbose;
    }

  if (mzVerbose>0) printf("%s:\n", fname);

  // Read problem.
  if (mzVerbose>0) printf("1. Read problem ... ");

  // samples
  _SciErr = getVarAddressFromPosition(pvApiCtx, 1, &X_addr); SCISVM_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, X_addr, &mX, &nX, &pdbl_X); SCISVM_ERROR;
  // labels
  _SciErr = getVarAddressFromPosition(pvApiCtx, 2, &Y_addr); SCISVM_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, Y_addr, &mY, &nY, &pdbl_Y); SCISVM_ERROR;

  if (nX!=nY)
    {
      Scierror(999,"%s: numbers of samples and labels should be the same. \n",fname);
      return 0;
    }

  x_space = mzSVMReadProblem(&prob, mX, nX, pdbl_X, pdbl_Y);

  if (mzVerbose>0) printf("OK!\n");

  // Read SVM model & parameters.
  if (mzVerbose>0) printf("2. Read SVM model ... ");

  // Read SVM model.
  if (Rhs >= 8) 
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 3, &alphaY_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, alphaY_addr, &malphaY, &nalphaY, &pdbl_alphaY); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 4, &SVs_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, SVs_addr, &mSVs, &nSVs, &pdbl_SVs); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 5, &Bias_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, Bias_addr, &mBias, &nBias, &pdbl_Bias); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 7, &dnSVs_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, dnSVs_addr, &mdnSVs, &ndnSVs, &pdbl_dnSVs); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 8, &dLabels_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, dLabels_addr, &mdLabels, &ndLabels, &pdbl_dLabels); SCISVM_ERROR;
      nClass  = ndLabels;
    } 
  else 
    {
      //this is only valid for two-class case
      _SciErr = getVarAddressFromPosition(pvApiCtx, 3, &alphaY_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, alphaY_addr, &malphaY, &nalphaY, &pdbl_alphaY); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 4, &SVs_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, SVs_addr, &mSVs, &nSVs, &pdbl_SVs); SCISVM_ERROR;
      _SciErr = getVarAddressFromPosition(pvApiCtx, 5, &Bias_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, Bias_addr, &mBias, &nBias, &pdbl_Bias); SCISVM_ERROR;
      int nAlphaY = nalphaY;
      int mAlphaY = malphaY;
      if (mAlphaY!=1) 
        {
          Scierror(999,"%s: This is a multclass problem, and you must input the parameter of nSV and nLabel. \n", fname);
          return 0;
        }
      pdbl_dnSVs  = (double *)MALLOC(2*sizeof(double));
      pdbl_dnSVs[0] = 0;
      pdbl_dnSVs[1] = 0;
      for (int count=0; count < nAlphaY; count++) 
        {
          if (pdbl_alphaY[count] < 0) 
            {
              pdbl_dnSVs[0]++;
            }
          else 
            {
              pdbl_dnSVs[1]++;
            }
        }	
      pdbl_dLabels = (double *)MALLOC(2*sizeof(double)); 
      if (pdbl_alphaY[0]<0)
        {
          pdbl_dLabels[0] = -1;
          pdbl_dLabels[1] = 1;	
        } 
      else 
        {
          pdbl_dLabels[0] = 1;
          pdbl_dLabels[1] = -1;	
        }
      nClass = 2;
    }

  mzSVMReadModel(&model, pdbl_dLabels, pdbl_dnSVs, pdbl_SVs, pdbl_alphaY, pdbl_Bias, nClass, mX);
	
  // Read input kernel parameters.
  mzSVMInitParameter(&(model.param));

  if (Rhs >= 6) 
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 6, &inPar_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, inPar_addr, &minPar, &ninPar, &pdbl_inPar); SCISVM_ERROR;
      if (minPar!=1)
        {
          Scierror(999,"%s: Kernel parameters is not a row vector!", fname);
          return 0;
        }
    mzSVMReadParameter(&(model.param), pdbl_inPar, ninPar);
  }

  if (model.param.gamma == 0) model.param.gamma = 1.0/mX;
  if (mzVerbose>0) printf("OK!\n");

  // Create output arguments.
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+1, 1, 1, &pdbl_CR); SCISVM_ERROR; // Correct Rate
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+2, 1, nX, &pdbl_DV); SCISVM_ERROR; // Decision Value
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+3, 1, nClass+1, &pdbl_dNs); SCISVM_ERROR; // Number of Samples
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+4, nClass+1, nClass+1, &pdbl_CM); SCISVM_ERROR; // Confusion Matrix
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+5, 1, nX, &pdbl_PredLabel); SCISVM_ERROR; // Predicted Labels

  // Classifying.
  if (mzVerbose>0) printf("3. Classify input patterns ... ");

  pdbl_CR[0] = 0;
  for(j=0; j<=nClass; j++) pdbl_dNs[j] = 0;
  for(i=0; i<(nClass+1)*(nClass+1); i++) pdbl_CM[i] = 0;

  for(i=0; i<nX; i++)
    {
      lTarget = mzSVMFindIndex(pdbl_dLabels, pdbl_Y[i], nClass);
      if (lTarget <0) lTarget = nClass;
      pdbl_PredLabel[i] =  svm_predict_values(&model, prob.x[i],&(pdbl_DV[i]));

      lPredict = mzSVMFindIndex(pdbl_dLabels, pdbl_PredLabel[i], nClass);

      if (lPredict<0)  lPredict = nClass;
      pdbl_CM[lTarget + lPredict*(nClass+1)]++;
      pdbl_dNs[lTarget]++;
    }

  for(i=0; i<=nClass; i++) 
    {
      if(pdbl_dNs[i]!=0) 
        {
          for(j=0; j<=nClass; j++) 
            {
              if (i == j) 
                {
                  pdbl_CR[0] += (pdbl_CM[i+j*(nClass+1)] / nX);
                }
              pdbl_CM[i+j*(nClass+1)] /= pdbl_dNs[i];
            }
        }
      else 
        {
          for(j=0; j<=nClass; j++) pdbl_CM[i+j*(nClass+1)] = (i==j) ? 1 : 0;
        }
    }

  if (mzVerbose>0) printf("OK!\n");

  if (mzVerbose>0) printf("4. Clear memory ... ");

  mzSVMDestroyModel(&model);

  if (mzVerbose>0) printf("OK!\n");
	
  if (Rhs < 8)
    {
      if (pdbl_dLabels) {FREE(pdbl_dLabels); pdbl_dLabels = NULL;}
      if (pdbl_dnSVs)   {FREE(pdbl_dnSVs);   pdbl_dnSVs   = NULL;}
    }

  if (prob.y)  {FREE(prob.y);  prob.y  = NULL;}
  if (prob.x)  {FREE(prob.x);  prob.x  = NULL;}
  if (x_space) {FREE(x_space); x_space = NULL;}

  LhsVar(1) = Rhs+1;
  if (Lhs>=2) LhsVar(2) = Rhs+2;
  if (Lhs>=3) LhsVar(3) = Rhs+3;
  if (Lhs>=4) LhsVar(4) = Rhs+4;
  if (Lhs>=5) LhsVar(5) = Rhs+5;

  return 0;
}
