/*  mzSVMCommon.h

    Head file of mzSVM common module.

    By Yi Zhao, OSU-EE, 01/20/2002 (version 2.00).
    By Yi Zhao, OSU-EE, 12/31/2001 (version 1.10).
    By Yi Zhao, OSU-EE, 12/22/2001 (version 1.00).
*/

#ifndef __mzSVMCommon_H_
#define __mzSVMCommon_H_

#include "svm.h"

#define SCISVM_ERROR if(_SciErr.iErr)           \
    {						\
      printError(&_SciErr, 0);			\
      return _SciErr.iErr;			\
    }

int  mzSVMFindIndex	(const double x[], double key, int size);
void mzSVMInitParameter	(svm_parameter * Parame);
void mzSVMReadParameter	(svm_parameter * Parame, const double Paras[],
                         int nPar);
void mzSVMSaveParameter	(const svm_parameter * Parame, double Paras[]);
void mzSVMReadWeight	(svm_parameter * Parame, const double Weight[], int nWT);
svm_node * mzSVMReadProblem	(svm_problem * prob, int mX, int nX, const double X[], const double Y[]);
void mzSVMReadModel	(svm_model * model, const double Labels[],
                         const double nSVs[], const double SVs[], const double alphaY[],
                         const double Bias[], int nClass, int mX);
void mzSVMSaveModel	(const svm_model * model, double Labels[],
                         double nSVs[], double SVs [], double alphaY[], double Bias[],
                         int mX);
void mzSVMDestroyModel	(svm_model * model);

#endif
