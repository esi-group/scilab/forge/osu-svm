function  [ClassRate, DecisionValue]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias, Parameters)
% [ClassRate, DecisionValue]= mexSVMClass(Samples, Labels, AlphaY, SVs, Bias, Parameters)
%
% Classify a group of patterns given a svm classifier.
%  please refer to http://www.csie.ntu.edu.tw/~cjlin/libsvm for more information
%
% Samples: all the input patterns. (a row of column vectors)
% Lables: the corresponding class labels for the input patterns in Samples.
%         Y(i) in {1, -1}. (a row vector)
% AlphaY: Alpha * Y, where Alpha is the non-zero Lagrange Coefficients
%                    Y is the corresponding labels.
% SVs : support vectors. That is, the patterns corresponding the non-zero
%       Alphas.
% Bias : the bias in the decision function, which is AlphaY*Kernel(SVs',x)-Bias.
% Parameters: the paramters required by the training algorithm. 
%             (a 10-element row vector)
%            +-----------------------------------------------------------------
%            |Kernel Type| Degree | Gamma | Coefficient | C |Cache size|epsilon| 
%            +-----------------------------------------------------------------
%				 -------------------------------------------+
%            | SVM Type |	nu (nu-svm) | loss tolerance |				
%				 -------------------------------------------+
%            where Kernel Type:
%                   0 --- Linear
%                   1 --- Polynomial: (Gamma*<X(:,i),X(:,j)>+Coefficient)^Degree
%                   2 --- RBF: (exp(-Gamma*|X(:,i)-X(:,j)|^2))
%                   3 --- Sigmoid: tanh(Gamma*<X(:,i),X(:,j)>+Coefficient)
%                  Gamma: If the input value is zero, Gamma will be set defautly as
%                         1/(max_pattern_dimension) in the function. If the input
%                         value is non-zero, Gamma will remain unchanged in the 
%                         function.
%                  C: Cost of the constrain violation (for C-SVC & C-SVR)
%                  Cache Size: as the buffer to hold the <X(:,i),X(:,j)> (in MB)
%                  epsilon: tolerance of termination criterion
%                  SVM Type: 
%                   0 --- c-SVM classifier
%                   1 --- nu-SVM classifier
%                   2 --- 1-SVM 
%                   3 --- c-SVM regressioner
%                  nu: the nu used in nu-SVM classifer (for 1-SVM and nu-SVM)
%						 loss tolerance: the epsilon in epsilon-insensitive loss function	
% ClassRate: the ration of properly classified pattern to the total number of
%            input patterns.
% DecisionValue: the output of the decision function.
%



 