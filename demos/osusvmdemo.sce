path = get_absolute_file_path('osusvmdemo.sce');

// OSU SVM Classifier Matlab Toolbox Demonstrations.

while 1
  demos = ['c_svcdemo.sce'
           'u_svcdemo.sce'
           'one_rbfdemo.sce'];
  clc();

  printf('------- OSU SVM CLASSIFIER TOOLBOX Demonstrations---\n\n');
  printf(' 1)  Demonstrations of using C-SVM Classifers.\n');
  printf(' 2)  Demonstrations of using u-SVM Classifiers.\n);
  printf(' 3)  Demonstration of uisng 1-SVM \n');
  printf(' 0)      Quit\n');
  
  // help osusvmdemo 
  n = input('Select a demo number: ');
  if ((n <= 0) | (n > 3)) then
    break;
  end
  demos = demos(n,:);
  eval('exec(''' + path + demos + ''')');
end
