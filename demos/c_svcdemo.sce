path = get_absolute_file_path('c_svcdemo.sce');

// OSU SVM Classifier Matlab Toolbox Demonstrations.

while 1
  demos= ['lindemo.sce'
          'poldemo.sce'
          'rbfdemo.sce'
          'clademo.sce'];
  clc();

  printf('------- OSU C-SVM CLASSIFIER TOOLBOX Demonstrations---\n\n');
  printf(' 1)  Construct a linear SVM Classifier and test it\n');
  printf(' 2)  Construct a nonlinear SVM Classifier (polynomial kernel) and test it\n');
  printf(' 3)  Construct a nonlinear SVM Classifier (rbf kernel) and test it\n');
  printf(' 4)  Classifier a set of input patterns\n');
  printf(' 0)      Return to upper level\n');

  // help c_svcdemo 
  n = input('Select a demo number: ');
  if ((n <= 0) | (n > 4)) then
    break;
  end
  demos = demos(n,:);
  eval('exec(''' + path + demos + ''')');
end

clc();
